'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Car extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Car.init({
    available: DataTypes.BOOLEAN,
    name: DataTypes.STRING,
    rentPerDay: DataTypes.INTEGER,
    size: DataTypes.ENUM('small', 'medium', 'large'),
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING,
    deletedBy: DataTypes.STRING,
    deletedAt: DataTypes.DATE,
  }, {
    sequelize,
    paranoid: true,
    timestamps: true,
    modelName: 'Car',
  });
  return Car;
};