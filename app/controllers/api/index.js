/**
 * @file contains entry point of controllers api module
 * @author Fikri Rahmat Nurhidayat
 */

const v1 = require("./v1");
 
module.exports = {
  v1,
};
 