const service = require("../../../services/usersService");
const bcrypt = require("bcrypt");
const salt = 10;

// enkripsi password
const encryptPassword = (password) => {
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, salt, (err, encryptedPassword) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(encryptedPassword);
    });
  });
};

// registrasi member
const register = async (req, res) => {
  const role = "member";
  const { email, password } = req.body;
  const encryptedPassword = await encryptPassword(password);
  const newUser = {
    role,
    email,
    password: encryptedPassword,
  };
  service
    .create(newUser)
    .then((user) => {
      res.status(201).json({
        status: "success",
        data: user,
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};

// registrasi admin
const registerAdmin = async (req, res) => {
  if (req.user.role != "superadmin") {
    res.status(401).json({
      status: "Unauthorized",
      message: "Anda tidak diizinkan untuk menambahkan admin",
    });
    return;
  }

  const role = "admin";
  const { email, password } = req.body;
  const encryptedPassword = await encryptPassword(password);
  const newUser = {
    role,
    email,
    password: encryptedPassword,
  };

  service
    .create(newUser)
    .then((user) => {
      res.status(201).json({
        status: "success",
        data: user,
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};

// menampilkan data semua user
const getUsers = (req, res) => {
  service
    .list()
    .then((users) => {
      res.status(200).json({
        status: "success",
        data: users,
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};

// menampilkan data user berdasarkan id
const getUserById = (req, res) => {
  service
    .get(req.params.id)
    .then((user) => {
      res.status(200).json({
        status: "success",
        data: user,
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};

// cek pengguna yang sedang login
const getCurrentUser = (req, res) => {
  res.status(200).json({
    status: "success",
    data: req.user,
  });
}


module.exports = {
  register,
  registerAdmin,
  getUsers,
  getUserById,
  getCurrentUser,
};
