const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const service = require("../../../services/usersService");
const secretKey = process.env.SECRET_KEY || "secret";

// mengecek password
const checkPassword = (encryptedPassword, password) => {
  return new Promise((resolve, reject) => {
    bcrypt.compare(password, encryptedPassword, (err, isValid) => {
      if (err) {
        reject(err);
        return;
      } 
      resolve(isValid);
    });
  });
};

// membuat token
const createToken = (payload) => jwt.sign(payload, secretKey, { expiresIn: "1h" });

// melakukan login
const login = async (req, res) => {
  const user = await service.getByEmail(req.body.email);

  if (!user) {
    res.status(404).json({
      message: "Email tidak ditemukan!",
    });
    return;
  }

  const isValid = await checkPassword(user.password, req.body.password);

  if (!isValid) {
    res.status(401).json({ message: "Password anda salah!" });
    return;
  }

  const { id, role, email, password } = user;
  const token = createToken({
    id,
    role,
    email,
    password,
  });

  res.status(201).json({
    status: "success",
    data: {
      id,
      role,
      email,
      token,
    },
  });
};

// authentication
const authorize = async (req, res, next) => {
  try {
    const bearerToken = req.headers.authorization;
    let token = "";
    if (bearerToken && bearerToken.startsWith("Bearer")) {
      token = bearerToken.split(" ")[1];
    } else {
      token = bearerToken;
    }
    const tokenPayload = jwt.verify(token, secretKey);
    req.user = await service.get(tokenPayload.id);
    next();
  } catch (err) {
    res.status(401).json({
      status: "Unauthorized",
      message: err.message,
    });
  }
};

module.exports = { login, authorize };