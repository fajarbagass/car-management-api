const service = require("../../../services/carsService");

// cek user untuk akses
const checkUser = (user) => {
  let message;
  if (user == "superadmin" || user == "admin") {
    message = {
      status: true,
      message: "Anda mendapatkan akses kesini",
    };
  } else {
    message = {
      status: false,
      message: "Anda tidak mendapatkan akses kesini",
    };
  }
  return message;
};

// menambah data mobil
const createCar = async (req, res) => {
  const { name, rentPerDay, size, available = true } = req.body;
  const { role, email } = req.user;
  const checkedUser = await checkUser(role);
  if (!checkedUser.status) return res.status(401).json(checkedUser);
  const newCar = {
    name,
    rentPerDay,
    size,
    available,
    createdBy: email,
  };
  service
    .createCar(newCar)
    .then((car) => {
      res.status(201).json({
        status: "success",
        data: car,
      });
    })
    .catch(err => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    })
};

// menampilkan semua data mobi
const getCar = (req, res) => {
  service
    .list()
    .then((cars) => {
      res.status(200).json({
        status: "success",
        data: cars,
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};

// menampilkan data mobil yang tersedia
const getAvailableCar = (req, res) => {
  service
    .availableCar()
    .then((cars) => {
      res.status(200).json({
        status: "success",
        data: cars,
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};

// mengupdate data mobil
const updateCar = async (req, res) => {
  const { id } = req.params;
  const { name, rentPerDay, size, available } = req.body;
  const { role, email } = req.user;
  const checkedUser = await checkUser(role);
  if (!checkedUser.status) return res.status(401).json(checkedUser);
  const updateCar = {
    name,
    rentPerDay,
    size,
    available,
    updatedBy: email,
  };
  service
    .updateCar(id, updateCar)
    .then(() => {
      res.status(200).json({
        status: "success",
        message: "Data berhasil diupdate",
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};

// menghapus data mobil
const deleteCar = async (req, res) => {
  const { id } = req.params;
  const { role, email } = req.user;
  const checkedUser = await checkUser(role);
  if (!checkedUser.status) return res.status(401).json(checkedUser);
  const deleted = {
    deletedBy: email,
  };
  service
    .deleteCar(id, deleted)
    .then(() => {
      res.status(200).json({
        status: "success",
        message: "Data berhasil dihapus",
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};

// memulihkan data mobil
const restoreCar = async (req, res) => {
  const { id } = req.params;
  const { role, email } = req.user;
  const checkedUser = await checkUser(role);
  if (!checkedUser.status) return res.status(401).json(checkedUser);
  const restore = {
    deletedBy: null,
    updatedBy: email
  };
  service
    .restore(id, restore)
    .then(() => {
      res.status(200).json({
        status: "success",
        message: "Data berhasil dipulihkan",
      });
    })
    .catch((err) => {
      res.status(422).json({
        status: "error",
        message: err.message,
      });
    });
};

module.exports = {
  createCar,
  getCar,
  getAvailableCar,
  updateCar,
  deleteCar,
  restoreCar
};