/**
 * @file contains entry point of controllers api module
 * @author Fikri Rahmat Nurhidayat
 */

const usersController = require("./usersController");
const authController = require("./authController");
const carsController = require("./carsController");
 
module.exports = {
  usersController,
  authController,
  carsController
};