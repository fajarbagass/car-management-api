/**
 * @file Bootstrap express.js server
 * @author Fikri Rahmat Nurhidayat
 */

const express = require("express");
const morgan = require("morgan");
const router = require("../config/routes");
const cors = require("cors");
const bodyParser = require("body-parser")
const swaggerUI = require("swagger-ui-express");
const yaml = require("js-yaml");
const fs = require("fs");
const swaggerDocument = yaml.load(fs.readFileSync("swagger.yaml", "utf8"));

const app = express();

/** Install request logger */
app.use(morgan("dev"));

app.use(cors());
app.use(bodyParser.urlencoded({ extended:true }));
app.use(bodyParser.json());

app.use("/api/v1/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerDocument));


/** Install Router */
app.use(router);


module.exports = app;
