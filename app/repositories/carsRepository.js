const { Car } = require("../models");

// create cars
const createCar = (value) => Car.create(value);

// find cars id
const find = (id) => Car.findByPk(id);

// get find all
const findAll = () => Car.findAll();

// get find by available true
const findAvailableCar = () => Car.findAll({where: {available: true}});

// update cars
const updateCar = (id, value) => Car.update(value, { where: { id } });

// delete cars
const deleteCar = (id) => Car.destroy({ where: { id } });

// restore cars
const restore = (id) => Car.restore({ where : { id } });

module.exports = {
  createCar,
  find,
  findAll,
  findAvailableCar,
  updateCar,
  deleteCar,
  restore
};
