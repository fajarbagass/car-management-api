const { User } = require("../models");

// create user
const create = (value) => User.create(value);

// get users find all
const findAll = () => User.findAll();

// get users find by id
const find = (id) => User.findByPk(id);

// get users find by email
const findOne = (email) => User.findOne({ where: { email } });

module.exports = {
  create,
  findAll,
  find,
  findOne,
};