const repository = require("../repositories/carsRepository");

// create cars
const createCar = (value) => repository.createCar(value);

// get all cars
const list = async () => {
  try {
    const cars = await repository.findAll();
    return cars;
  } catch (err) {
    throw err;
  }
};

// get find cars by available
const availableCar = async (key, value) => {
  try {
    const cars = await repository.findAvailableCar();
    return cars;
  } catch (err) {
    throw err;
  }
};

// update cars
const updateCar = async (id, value) => {
  try {
    let cars = await repository.find(id);
    if (cars === null) {
      throw new Error('Data tidak ditemukan');
    } else {
      repository.updateCar(id, value);      
    }
  } catch (err) {
    throw err;
  }
};

// delete cars
const deleteCar = async (id, user) => {
  try {
    let cars = await repository.find(id);
    if (cars === null) {
      throw new Error('Data tidak ditemukan');
    } else {
      await repository.updateCar(id, user);
      repository.deleteCar(id);
    }
  } catch (err) {
    throw err;
  }
};

// restore cars
const restore = async (id, user) => {
  try {
    let cars = await repository.find(id);
    if (cars != null) {
      throw new Error('Data tidak ditemukan');
    } else {
      await repository.restore(id);
      repository.updateCar(id, user);
    }
  } catch (err) {
    throw err;
  }
};

module.exports = {
  createCar,
  list,
  availableCar,
  updateCar,
  deleteCar,
  restore
};
