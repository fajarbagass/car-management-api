const repository = require("../repositories/usersRepository");

// create users
const create = (value) => repository.create(value);

// get all users
const list = async () => {
  try {
    const user = await repository.findAll();
    return user;
  } catch (err) {
    throw err;
  } 
};

// get users by id
const get = (id) => repository.find(id);

// get users by email
const getByEmail = (email) => repository.findOne(email);

module.exports = {
  create,
  list,
  get,
  getByEmail,
};