const express = require("express");
const controllers = require("../app/controllers");
const authController = controllers.api.v1.authController;
const usersController = controllers.api.v1.usersController;
const carsController = controllers.api.v1.carsController;
const apiRouter = express.Router();

/**
 * TODO: Implement your own API
 *       implementations
 */

// create member
apiRouter.post("/api/v1/register/member", usersController.register);

// create admin
apiRouter.post("/api/v1/register/admin",authController.authorize, usersController.registerAdmin);

// login all user
apiRouter.post("/api/v1/login", authController.login);

// Current user login
apiRouter.get("/api/v1/users", authController.authorize, usersController.getCurrentUser);

// Create cars
apiRouter.post("/api/v1/cars/create", authController.authorize, carsController.createCar);

// Update cars
apiRouter.put("/api/v1/cars/update/:id", authController.authorize, carsController.updateCar);

// Soft Delete cars
apiRouter.delete("/api/v1/cars/delete/:id",authController.authorize, carsController.deleteCar);

// Restore cars data
apiRouter.put("/api/v1/cars/restore/:id", authController.authorize, carsController.restoreCar);

// get cars by available true
apiRouter.get("/api/v1/cars", carsController.getAvailableCar);


module.exports = apiRouter;
